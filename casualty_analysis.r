# weather model and graph
weather_model <- glm(human_casualty ~ weather_condition, family = binomial(link = "logit"), data = working_data)

coef_temp <- data.frame(summary(weather_model)$coefficients)

coef_temp %>%
  mutate(coefficients = str_remove(row.names(coef_temp), "weather_condition")) %>%
  mutate(coefficients = case_when(
    str_detect(coefficients, "Intercept") ~ "Dry",
    T ~ coefficients
  )) %>%
  ggplot(aes(x = Pr...z.., y = coefficients)) + geom_point(shape=1, size=3, color="red2") -> p1

p1 + geom_vline(xintercept = 0.01, colour="green3", size=1.5) + geom_vline(xintercept = 0.05, colour="orange", size = 1.5) + xlab("p-value")

# surface model and graph
surface_model <- glm(human_casualty ~ surface_condition, family = binomial(link = "logit"), data = working_data)

coef_temp <- data.frame(summary(surface_model)$coefficients)

coef_temp %>%
  mutate(coefficients = str_remove(row.names(coef_temp), "surface_condition")) %>%
  mutate(coefficients = case_when(
    str_detect(coefficients, "Intercept") ~ "Dry",
    T ~ coefficients
  )) %>%
  ggplot(aes(x = Pr...z.., y = coefficients)) + geom_vline(xintercept = 0.01, colour="green3", size=1.5) + geom_vline(xintercept = 0.05, colour="orange", size = 1.5) -> p2

p2 <- p2 + geom_point(shape=1, size=3, color="red2") + xlab("p-value")

majorcause_model <- glm(human_casualty ~ cause_factor, family = binomial(link = "logit"), data = working_data)

coef_temp <- data.frame(summary(majorcause_model)$coefficients)

coef_temp %>%
  mutate(coefficients = str_remove(row.names(coef_temp), "cause_factor")) %>%
  mutate(coefficients = case_when(
    str_detect(coefficients, "Intercept") ~ "animal",
    T ~ coefficients
  )) %>%
  ggplot(aes(x = Pr...z.., y = coefficients)) + geom_vline(xintercept = 0.01, colour="green3", size=1.5) + geom_vline(xintercept = 0.05, colour="orange", size = 1.5) -> p3

p3 + geom_point(shape=1, size=3, color="red2") + xlab("p-value")
